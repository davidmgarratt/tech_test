package com.sauceweb.steps;

import com.sauceweb.pages.HomePage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageSteps {
    HomePage homepage = new HomePage();


    @Given("^User navigates to SauceWeb HomePage$")
    public void userIsOnHomepage() {
        homepage.verifyUserOnHomepage();
    }

    @When("^Customer enter Username details$")
    public void enterUserName() throws Exception {
        homepage.enterUserNameDetails();
    }

    @And("^Customer enter Password details$")
    public void enterPassword() throws Exception {
        homepage.enterPasswordDetails();
    }
    @And("^Click on Login button$")
    public void clickOnLoginBtn() throws Exception {
        homepage.clickOnLogin();
    }

    @Then("^User successfully logged into the website$")
    public void verifyLogin() throws Exception {
        homepage.loginConfirm();
    }
}

