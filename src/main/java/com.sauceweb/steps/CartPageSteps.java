package com.sauceweb.steps;

import com.sauceweb.pages.CartPage;
import cucumber.api.java.en.Then;

public class CartPageSteps {
   CartPage cartpage = new CartPage();

    @Then("^Verify \"([^\"]*)\" product on cart page$")
    public void verifyAddToCart(String productName) throws Exception {
        cartpage.verifyAddedProductToCartPage(productName);
    }

}
