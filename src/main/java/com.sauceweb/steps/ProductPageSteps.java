package com.sauceweb.steps;

import com.sauceweb.pages.ProductPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ProductPageSteps {
    ProductPage productpage = new ProductPage();

    @Then("^Select high-low as the price filter$")
    public void filterHighLow() throws Exception {
        productpage.filterOptions();
    }


    @And("^Validate highest price product listed first$")
    public void verifyHighPriceProduct() throws Exception {
        productpage.filterSelectionDisplay();
    }

    @And("^Add the lowest price product to the basket$")
    public void addLowestPrice() throws Exception {
        productpage.addStandardUserProduct();
    }

}
