package com.sauceweb.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonUtils {

    public static void clickOnElement(WebElement element) throws Exception {
        element.click();
    }

    public static void sendKeysTo(WebElement element, String text) throws Exception {
        element.clear();
        element.sendKeys(text);
    }
}
