package com.sauceweb.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.sauceweb.utils.DriverUtils.getDriver;
import static com.sauceweb.utils.CommonUtils.clickOnElement;



public class CartPage {
    public CartPage(){
        PageFactory.initElements(getDriver(),this);
    }


    @FindBy(id = "shopping_cart_container")
    private WebElement btnShoppingCart;

    public void verifyAddedProductToCartPage(String productName) throws Exception {
        clickOnElement(btnShoppingCart);


    }

}
