package com.sauceweb.pages;

import com.saucedemo.data.ProductDataBean;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.sauceweb.utils.CommonUtils.clickOnElement;
import static com.sauceweb.utils.DriverUtils.getDriver;


public class ProductPage {
    ProductDataBean productDetailsBean = new ProductDataBean();

    public ProductPage() {
        PageFactory.initElements(getDriver(), this);
    }
    @FindBy(id = "add-to-cart-sauce-labs-onesie")
    private WebElement btnLowestProduct;

    public void addStandardUserProduct() throws Exception {
        clickOnElement(btnLowestProduct);
    }
}
