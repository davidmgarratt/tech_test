package com.sauceweb.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import static com.sauceweb.utils.CommonUtils.clickOnElement;
import static com.sauceweb.utils.CommonUtils.sendKeysTo;
import static com.sauceweb.utils.DriverUtils.getDriver;



public class HomePage {

    public HomePage() {
        PageFactory.initElements(getDriver(), this);
    }

    @FindBy(id = "user-name")
    private WebElement txtUserName;

    @FindBy(id = "password")
    private WebElement txtPassword;

    @FindBy(id = "shopping_cart_container")
    private WebElement imgShoppingCart;

    @FindBy(className = "login_logo")
    private  WebElement imgLoginLogo;

    @FindBy(id = "login-button")
    private WebElement btnSignIn;

    public void verifyUserOnHomepage(){;
    }

    public void enterUserNameDetails() throws Exception {
        sendKeysTo(txtUserName,"standard_user");
    }

    public void enterPasswordDetails() throws Exception {
        sendKeysTo(txtPassword,"secret_sauce");
    }

    public void clickOnLogin() throws Exception {
     clickOnElement(btnSignIn);
    }

    public void loginConfirm() throws Exception {
    }

}
