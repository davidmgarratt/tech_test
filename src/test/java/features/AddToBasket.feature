@basketflow
Feature: SauceWeb Add to Basket flow

  @sauce_web_basket
  Scenario: Add the lowest price product to the basket

    Given User navigates to SauceWeb HomePage
    When Customer enter Username details
    And Customer enter Password details
    And Click on Login button
    Then User successfully logged into the website
    And Add the lowest price product to the basket
    Then Verify "Sauce Labs Onesie" product on cart page