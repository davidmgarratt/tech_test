package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(tags = "@basketflow", glue = "com.sauceweb.steps",
        features = "src/test/java/features/")
public class TestRunner {

}
