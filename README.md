### Software Development Engineer in Test Interview

#### Task 
Using a retail website (Amazon, Takealot, Flipkart etc etc)
User should be able to add a product to the basket and verify the correct product is added

Using Sauce Demo Website

#### Objectives
- Create feature file using Gherkin syntax
- Create step definitions and link these to the feature file
- Write implementation which links both pieces up
- Create TestRunner to run test 
- Make sure the test passes

Good Luck!
